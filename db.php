<?php

require "Medoo.php";


$database = new Medoo\Medoo([
    'database_type' => 'sqlite',
    'database_file' => __DIR__ . '/' . 'info.sqlite3'
]);

function getName($n)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }

    return $randomString;
}

function is_allowed($user_id)
{
    global $database;
    $res = $database->select('users', [
        "allowed",
    ],
        ['user_id' => $user_id]);

    return $res[0]['allowed'];
}

function grant_perm($user_id)
{
    global $database;
    $database->update('users', ["allowed" => 1], ['user_id' => $user_id]);
}

function init_user($user_id,$chat_id)
{
    global $database;
    $database->insert('users', ['user_id' => $user_id,'chat_id'=>$chat_id]);
}

function set_gender($user_id, $gender)
{
    global $database;
    $database->update('users', ["gender" => $gender], ['user_id' => $user_id]);

}

function get_gender($user_id)
{
    global $database;

    $res = $database->select('users', ["gender"], ['user_id' => $user_id]);
    return $res[0]['gender'];
}

function set_code($gender, $user_id)
{
    $code = $gender . getName(4);
    global $database;
    $database->update('users', ["code" => $code], ['user_id' => $user_id]);
}

function get_code($user_id)
{
    global $database;

    $res = $database->select('users', ["code"], ['user_id' => $user_id]);
    return $res[0]['code'];
}

function reply($user_id, $msg_id)
{
    global $database;
//    insert msg id for user and set flag to 0
    $database->insert('reply', [
        "user_id" => $user_id,
        "msg_id" => $msg_id,
        "is_replied" => 0
    ]);


}

function replied_msg_id($user_id)
{
    global $database;
    $res = $database->select('reply', [
        "msg_id",
    ],
        ['user_id' => $user_id]);

    return $res[0]['msg_id'];
}

function is_replied($user_id)
{
    global $database;
    $res = $database->select('reply', [
        "is_replied",
    ],
        ['user_id' => $user_id]);

    return $res[0]['is_replied'];

}

function unpend_user_reply($user_id)
{
    global $database;
    $database->update('reply', ['is_replied' => 1], ["user_id" => $user_id]);
}