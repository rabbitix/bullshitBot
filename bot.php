<?php
include "config.php";

$bot = new Telegram($info->token);
$content['chat_id'] = $bot->ChatID();

$user_id = $bot->UserID();
$trns_wrd = 'ترنس🌈';
$Boy = 'پسر🙆🏻‍♂️';
$Girl = 'دختر🙋🏻‍♀️';
$dontWantToSay = '🙅🏻‍♀️نمیخوام بگم🙅🏻‍♂️';
$gender_keys = $bot->buildKeyBoard(
    [
        [$bot->buildKeyboardButton($trns_wrd)],
        [$bot->buildKeyboardButton($Boy)],
        [$bot->buildKeyboardButton($Girl)],
        [$bot->buildKeyboardButton($dontWantToSay)],

    ], true, true
);
if (preg_match($pattern, $bot->Text())) {
    $content['text'] = $bot->Text();
    $bot->sendMessage($content);
}

if ($bot->Text() == '/start') {
    if (is_allowed($user_id)) {
        $txt = "خوش اومدی ";
        $content['text'] = $txt;
        $bot->sendMessage($content);
    } else {
        init_user($user_id, $bot->ChatID());

        $txt = "خوش اومدی. برا اینکه بشه ازم استفاده کنی باید جنسیتت رو مشخص کنی.اگه نمیخای بگی میتونی کلید نمیخام رو بزنی :) (این تنظیم بعدا قابل تغییر نیست)";
        $content['reply_markup'] = $gender_keys;
        $content['text'] = $txt;
        $bot->sendMessage($content);
    }

} elseif (!is_allowed($user_id)) {
//    so need to determain gender

    switch ($bot->Text()) {
        case $trns_wrd :
        {
            set_gender($user_id, "T");
            set_code("T", $user_id);
            $code = get_code($user_id);
            $txt = "خب حالا میتونی از من استفاده کنی..من بصورت خودکار کد زیر رو تو همه ی پیام هات اضافه میکنم اینطوری  پیام هایی که تو کانال فرستادی با هشتگت در دسترسه..الان میتونی برام پیام بفرستی :) " . PHP_EOL . "`$code`";
            $content['text'] = $txt;
            $content['parse_mode'] = "MARKDOWN";
            $content['reply_markup'] = $bot->buildKeyBoardHide();
            $bot->sendMessage($content);
            grant_perm($user_id);
            break;
        }
        case $Boy :
        {
            set_gender($user_id, "M");
            set_code("M", $user_id);
            $code = get_code($user_id);
            $txt = "خب حالا میتونی از من استفاده کنی..من بصورت خودکار کد زیر رو تو همه ی پیام هات اضافه میکنم اینطوری  پیام هایی که تو کانال فرستادی با هشتگت در دسترسه..الان میتونی برام پیام بفرستی :) " . PHP_EOL . "`$code`";
            $content['text'] = $txt;
            $content['parse_mode'] = "MARKDOWN";
            $content['reply_markup'] = $bot->buildKeyBoardHide();
            $bot->sendMessage($content);
            grant_perm($user_id);
            break;
        }
        case $Girl :
        {
            set_gender($user_id, "F");
            set_code("F", $user_id);
            $code = get_code($user_id);
            $txt = "خب حالا میتونی از من استفاده کنی..من بصورت خودکار کد زیر رو تو همه ی پیام هات اضافه میکنم اینطوری  پیام هایی که تو کانال فرستادی با هشتگت در دسترسه..الان میتونی برام پیام بفرستی :) " . PHP_EOL . "`$code`";
            $content['text'] = $txt;
            $content['parse_mode'] = "MARKDOWN";
            $content['reply_markup'] = $bot->buildKeyBoardHide();
            $bot->sendMessage($content);
            grant_perm($user_id);
            break;
        }
        case $dontWantToSay :
        {
            set_gender($user_id, "N");
            set_code("N", $user_id);
            $code = get_code($user_id);
            $txt = "خب حالا میتونی از من استفاده کنی..من بصورت خودکار کد زیر رو تو همه ی پیام هات اضافه میکنم اینطوری  پیام هایی که تو کانال فرستادی با هشتگت در دسترسه..الان میتونی برام پیام بفرستی :) " . PHP_EOL . "`$code`";
            $content['text'] = $txt;
            $content['parse_mode'] = "MARKDOWN";
            $content['reply_markup'] = $bot->buildKeyBoardHide();
            $bot->sendMessage($content);
            grant_perm($user_id);
            break;
        }
        default:
        {
            $txt = "قربون شکل 🌙‌ـت بشم، تا جنسیتتو نگی پیاماتو نمیفرستم! میتونی بزنی نمیخام بگم..اونم قبوله :)
/start بزن تا بتونی جنسیتت رو انتخاب کنی";
            $content['text'] = $txt;


            $content['reply_markup'] = $gender_keys;
            $bot->sendMessage($content);
            break;
        }
    }

} elseif (is_allowed($user_id) and $bot->getUpdateType() != $bot::CHANNEL_POST) {
    if ($bot->isUserSubscriber($bot->ChatID(), $user_id)) {
        $ok_txt = 'پیامت فرستاده شد💋❤️';
        $content['chat_id'] = $bot->ChatID();
        $content['text'] = $ok_txt;
        $bot->sendMessage($content);

        $content['chat_id'] = $info->channel_id;
        $code = get_code($user_id);
        $type = $bot->getUpdateType();
        $api = 'send' . ucwords($type);
        $content[$type] = $bot->FileId($type);
        $cap = $bot->Caption();
        $content['caption'] = isset($cap) ? $bot->Caption() . PHP_EOL . "#$code" : PHP_EOL . "#$code";
        if ($bot->Text()) {
            $content['text'] = $bot->Text() . PHP_EOL . "#$code";
        }
        $bot->endpoint($api, $content);

    } else {
        $content['text'] = "تو کانال زیر عضو شو تا هم پیام هایی که میفرستی ببینی، هم بتونی از من استفاده کنی D: ";
        $bot->sendMessage($content);

    }
}

