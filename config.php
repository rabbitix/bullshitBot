<?php


$file = file_get_contents(__DIR__ . '/info.json');
$info = json_decode($file);
require "Telegram.php";
$content = array();
include "db.php";
$pattern = "/\/start \w{4}/";


$admins = [];
function limit($status = null)
{
//    1 -> open
//    2 -> close
    $folder = __DIR__ . "/limit_status/";
    mkdir($folder);
    $path = $folder . "status.txt";
    if (is_null($status)) {
        return file_get_contents($path);
    }
    return file_put_contents($path, $status);
}